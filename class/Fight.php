<?php

class Fight {
    
    /**
     * Number of rounds
     * @var type 
     */
    protected $rounds;
    /**
     * First Fighter
     * @var type 
     */
    protected $fighter1 = null;
    /**
     * Second Fighter
     * @var type 
     */
    protected $fighter2 = null;
    /**
     * Fight Log
     * @var type 
     */
    protected $fightLog = array();

    public function __construct($rounds) {
        $this->rounds = $rounds;
    }

    /**
     * Set fighters
     * @param Hero_instance $fighter
     * @return $this
     */
    public function setFighters(Hero_instance $fighter) {
        if(is_null($this->fighter1)) {
            $this->fighter1 = $fighter;
        } else {
            $this->fighter2 = $fighter;
        }
        return $this;
    }    
    
    /**
     * Return Fighters as array
     * @return type
     */
    public function getFighters() {
        return array($this->fighter1, $this->fighter2);
    }

    /**
     * Fight trigger
     * @return type
     */
    public function fight() {
        $this->firstAtac();
        $i = 0;
        while ($i < $this->rounds) {
            if($this->fighter1->getHealth() > 0 && $this->fighter2->getHealth() > 0) {
                if($i%2==0){
                    $this->atac($this->fighter1, $this->fighter2);
                } else {
                    $this->atac($this->fighter2, $this->fighter1);
                }
            } else {
                break;
            }
            $i++;    
        }
        if ($this->fighter1->getHealth() > $this->fighter2->getHealth()) {
            $this->addlog('<b>' . $this->fighter1->getName() . ' won this fight!</b>');
        } else {
            $this->addlog('<b>' . $this->fighter2->getName() . ' won this fight!</b>');
        }
        return array($this->fighter1, $this->fighter2);
    }
    
    /**
     * Get Fight Logs
     * @return type
     */
    public function getFightLog() {
        return $this->fightLog;
    }

    /**
     * Process the attack
     * @param type $attacker
     * @param type $defender
     * @return type
     */
    protected function atac($attacker, $defender) {
        $fight = intval($attacker->getStrength() - $defender->getDefence());
        if($attacker->checkSkills()) {
            $fight = $attacker->callOfence($fight);
        }
        if($defender->checkSkills()) {
            $fight = $defender->callDefence($fight);
        }
        if ($fight > 0 && $this->isLucky($defender->getLuck())) {
            $defender->updateHealth($fight);
        }
        $this->addlog('<b>' . $attacker->getName() . '</b> attak <b>' . $defender->getName() . '</b> and made damage: ' . $fight . '. Defender <b>' . $defender->getName() . '</b> remeins with: ' . $defender->getHealth());
        return $defender;
    }
    
    /**
     * Add Fight Logs
     * @param type $message
     */
    protected function addlog($message) {
        $this->fightLog[] = $message;
    }

    /**
     * Determine who attack first
     */
    protected function firstAtac() {
        if($this->fighter1->getSpeed() < $this->fighter2->getSpeed()) {
            $this->switchFighters();
        } if($this->fighter1->getSpeed() == $this->fighter2->getSpeed() && $this->fighter1->getLuck() < $this->fighter2->getLuck()) {
            $this->switchFighters();
        }
    }
    
    /**
     * Switch Fighters places
     */
    protected function switchFighters() {
        $tmp = $this->fighter1;
        $this->fighter1 = $this->fighter2;
        $this->fighter2 = $tmp;
    }

    /**
     * Check if player is lucky
     * @param type $luck
     * @return boolean
     */
    protected function isLucky($luck) {
        $chance = rand(0, 100);
        if ($chance < $luck) {
            return true;
        }
        return false;
    }
}
