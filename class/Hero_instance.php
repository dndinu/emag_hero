<?php

interface Hero_instance {
    
    public function __construct($default);
    
    public function useDefault(); 
        
    public function getHealth(); 
    
    public function getSkills(); 
    
    public function getName(); 
    
    public function getSpeed(); 
    
    public function getLuck(); 
    
    public function getStrength(); 
    
    public function getDefence(); 
    
    public function updateHealth($value); 
    
    public function checkSkills(); 
    
    public function callOfence($param); 
    
    public function callDefence($param);

}

