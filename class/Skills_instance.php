<?php

interface Skills_instance {
    public function __construct($skills);
    
    public function getSkills();
    
    public function callSkill($skillName, $toProcess);
    
    public function getSkillsByType();
}
