<?php

require 'class/Hero_instance.php';
require 'class/Skills.php';

class Hero implements Hero_instance{
    /**
     * Determine if hero is create base on relative values or static values
     * @var type 
     */
    protected $default = false;
    /**
     * Hero characteristics
     * @var type 
     */
    protected $name;
    protected $health;
    protected $strength;
    protected $defence;
    protected $speed;
    protected $luck;
    protected $skills = null;
    /**
     * Skill processor
     * @var type 
     */
    protected $skillsProcessor;


    public function __construct($default) {
        $this->useDefault($default);
    }
    
    /**
     * Set Hero creation type
     */
    public function useDefault() {
        $this->default = true;
    }
        
    public function getHealth() {
        return $this->health;
    }
    
    public function getSkills() {
        return $this->skills;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function getSpeed() {
        return $this->speed;
    }
    
    public function getLuck() {
        return $this->luck;
    }
    
    public function getStrength() {
        return $this->strength;
    }
    
    public function getDefence() {
        return $this->defence;
    }    
    
    /**
     * Process Health parameters changes
     * @param type $value
     * @return $this
     */
    public function updateHealth($value) {
        $this->health = $this->health - $value;
        if ($this->health < 0) {
            $this->health = 0;
        }
        return $this;
    }
    
    /**
     * Check if Hero has skills
     * @return boolean
     */
    public function checkSkills() {
        if (is_null($this->skills)) {
            return false;
        }
        return true;
    }
    
    /**
     * Call hero Offence Skills
     * @param type $param
     * @return type
     */
    public function callOfence($param) {
        foreach ($this->skills['ofence'] as $key => $val) {
            $param = $param + $this->skillsProcessor->callSkill($val, $param);
        }
        return $param;
    }
     /**
      * Call hero Defence Skills
      * @param type $param
      * @return type
      */
    public function callDefence($param) {
        foreach ($this->skills['defence'] as $key => $name) {
            $param = $param + $this->skillsProcessor->callSkill($name, $param);
        }
        return $param;
    }

    /**
     * Create hero
     * @param type $heroName
     * @param type $heroData
     */
    public function create($heroName, $heroData) {
        $this->setName($heroName);
        $this->setHealth($heroData['health']);
        $this->setStrength($heroData['strength']);
        $this->setDefence($heroData['defence']);
        $this->setSpeed($heroData['speed']);
        $this->setLuck($heroData['luck']);
        $this->setSkills($heroData['skills']);
    }
    
    /**
     * Call Skills object
     * @param type $skills
     */
    protected function setSkills($skills) {
        if($this->hasSkils($skills)) {
            $this->skillsProcessor = new Skills($skills);
            $this->skills = $this->skillsProcessor->getSkillsByType();
        }
    }

    /**
     * Check if hero has skills associate
     * @param type $skils
     * @return type
     */
    protected function hasSkils($skils) {
        return !empty($skils);
    }

    protected function setName($name) {
        $this->name = $name;
        return $this;
    }
    
    protected function setHealth($health) {
        if ($this->default) {
            $this->health = $this->getRandValue($health);
        } else {
            $this->health = $health;
        }
        return $this;
    }
    
    protected function setSpeed($speed) {
        if ($this->default) {
            $this->speed = $this->getRandValue($speed);
        } else {
            $this->speed = $speed;
        }
        return $this;
    }
    
    protected function setLuck($luck) {
        if ($this->default) {
            $this->luck = $this->getRandValue($luck);
        } else {
            $this->luck = $luck;
        }
        return $this;
    }
    
    protected function setStrength($strength) {
        if ($this->default) {
            $this->strength = $this->getRandValue($strength);
        } else {
            $this->strength = $strength;
        }
        return $this;
    }
    
    protected function setDefence($defence) {
        if ($this->default) {
            $this->defence = $this->getRandValue($defence);
        } else {
            $this->defence = $$defence;
        }
        return $this;
    }
   
    /**
     * Returns a random values between min and max
     * @param type $param
     * @return type.
     */
    protected function getRandValue($param) {
        return rand($param['min'], $param['max']);
    }
}


