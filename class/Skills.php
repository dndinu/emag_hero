<?php

require_once 'class/Skills_instance.php';
class Skills implements Skills_instance {
    
    /**
     * Define available skills details
     * @var type 
     */
    protected $skils = array(
        'rapid_strike' => array(
            'name'  => 'RapidStrike',
            'type'  => 'ofence',
            'value' => 0
        ),
        'magic_sheeld' => array (
            'name'  => 'MagicSheeld',
            'type'  => 'defence',
            'value' => 0
        )
    );
    
    /**
     * Define available skills
     * @var type 
     */
    protected $skillByType = array(
        'offence', 'defence'
    );

    public function __construct($skills) {
        $this->setSkills($skills);
        return $this;
    }
    
    /**
     * Call to execution a specific skill
     * @param type $skillName
     * @param type $toProcess
     * @return type
     */
    public function callSkill($skillName, $toProcess) {
        $value = $this->skils[$skillName]['value'];
        $process = $this->skils[$skillName]['name'];
        return $this->$process($value, $toProcess);
    }
    
    /**
     * Returns skills by type
     * @return type
     */
    public function getSkillsByType() {
        return $this->skillByType;
    }

    /**
     * Set skills value
     * @param type $skils
     */
    protected function setSkills($skils) {
        foreach ($skils as $name => $value) {
            $this->skils[$name]['value'] = $value;
            $this->skillByType[$this->skils[$name]['type']][] = $name;
        }
    }

    public function getSkills() {
        return $this->skils;
    }

    /**
     * Skill functionality definition
     * @param type $SkillParam
     * @param type $toProcess
     * @return type
     */
    protected function RapidStrike($SkillParam, $toProcess) {
        if ($this->isLucky($SkillParam)) {
            return $SkillParam * 2;
        }
        return $SkillParam;
    }
    
    /**
     * Skill functionality definition
     * @param type $SkillParam
     * @param type $toProcess
     * @return type
     */
    protected function MagicSheeld($SkillParam, $toProcess) {
        if ($this->isLucky($SkillParam)) {
            return $SkillParam / 2;
        }
        return $SkillParam;
    }
    
    /**
     * Determine if hero will be lucky with his skill
     * @param type $luck
     * @return boolean
     */
    protected function isLucky($luck) {
        $chance = rand(0, 100);
        if ($chance < $luck) {
            return true;
        }
        return false;
    }
}
