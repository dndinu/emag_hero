<?php

require_once 'config.php';
require_once 'class/Hero.php';
require_once 'class/Fight.php';

$fight = new Fight(8);

foreach (HEROS as $key => $val) {
    $fighter = new Hero(true);
    $fighter->create($key, $val);
    $fight->setFighters($fighter);
}
?>
<!DOCTYPE html>
<html>
<head>
<title>eMAG'S Hero</title>
</head>
<body>
    <div>
        <h1 style="text-align: center">eMAG'S Hero</h1>
    </div>
    <div style="width: 100%; display: flex;">
        <?php foreach ($fight->getFighters() as $val) { ?>
        <div style="width: 50%; text-align: center"><b><h3><?php echo $val->getName(); ?></h3></b></div>
        <?php } ?>
    </div>
    <div style="width: 100%; display: flex;">
        <?php foreach ($fight->getFighters() as $val) { ?>
        <div style="width: 50%; text-align: center"><b><h3><?php echo 'Health: ' . $val->getHealth(); ?></h3></b></div>
        <?php } ?>
    </div>
    <div style="width: 100%; display: flex;">
        <?php foreach ($fight->getFighters() as $val) { ?>
        <div style="width: 50%; text-align: center"><b><h3><?php echo 'Strength: ' . $val->getStrength(); ?></h3></b></div>
        <?php } ?>
    </div>
    <div style="width: 100%; display: flex;">
        <?php foreach ($fight->getFighters() as $val) { ?>
        <div style="width: 50%; text-align: center"><b><h3><?php echo 'Defence: ' . $val->getDefence(); ?></h3></b></div>
        <?php } ?>
    </div>
    <div style="width: 100%; display: flex;">
        <?php foreach ($fight->getFighters() as $val) { ?>
        <div style="width: 50%; text-align: center"><b><h3><?php echo 'Speed: ' . $val->getSpeed(); ?></h3></b></div>
        <?php } ?>
    </div>
    <div style="width: 100%; display: flex;">
        <?php foreach ($fight->getFighters() as $val) { ?>
        <div style="width: 50%; text-align: center"><b><h3><?php echo 'Luck: ' . $val->getLuck(); ?></h3></b></div>
        <?php } ?>
    </div>
    
    <?php $fight->fight(); ?>
    
    <?php foreach ($fight->getFightLog() as $val) { ?>
        <div style="width: 100%; display: flex;">
            <div style="width: 100%; text-align: center"><?php echo 'Log: ' . $val; ?></div>
        </div>
    <?php } ?>

    <div style="width: 100%;text-align: center">
        <form method="post">
            <input type="submit" name="submit" value="New Fight">  
        </form>
    </div>
</body>
</html>

