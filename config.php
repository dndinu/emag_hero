<?php

define('HEROS', 
    array(
        'orderus' => array (
            'health' => array(
                'min' => 70,
                'max' => 100
            ),
            'strength' => array(
                'min' => 70,
                'max' => 80
            ),
            'defence' => array(
                'min' => 45,
                'max' => 55
            ),
            'speed' => array(
                'min' => 40,
                'max' => 50
            ),
            'luck' => array(
                'min' => 10,
                'max' => 30
            ),
            'skills' => array(
                'rapid_strike' => 10,
                'magic_sheeld' => 20
            ),
        ),
        'beast' => array (
            'health' => array(
                'min' => 60,
                'max' => 90
            ),
            'strength' => array(
                'min' => 60,
                'max' => 90
            ),
            'defence' => array(
                'min' => 40,
                'max' => 60
            ),
            'speed' => array(
                'min' => 40,
                'max' => 60
            ),
            'luck' => array(
                'min' => 25,
                'max' => 40
            ),
            'skills' => array()
        )
    )
);